import React from "react"
import PropTypes from "prop-types"
import "./Tile.scss"

const Tile = ({ num, onClick }) => (
  <div className={`tile pos-${num}`} data-position={num} onClick={onClick} />
)

Tile.propTypes = {
  num: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired
}

export default Tile
