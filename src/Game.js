import React from "react"
import "./Game.scss"

import Tile from "./Tile"

class Game extends React.Component {
  constructor() {
    super()

    this.state = { sequence: [1, 2, 3, 4, 5, 6, 7, 8, 9] }
  }

  // Randomise array values
  _shuffle(arr) {
    return arr.sort(() => Math.random() - 0.5)
  }

  // Produce an array of allowed values based on empty space
  _allowedNumbers(position) {
    let allowed = []

    // Allowed up
    if (position >= 4) {
      allowed.push(position - 3)
    }
    // Allowed right
    if (position % 3 !== 0) {
      allowed.push(position + 1)
    }
    // Allowed down
    if (position <= 6) {
      allowed.push(position + 3)
    }
    // Allowed left
    if (position % 3 !== 1) {
      allowed.push(position - 1)
    }

    return allowed
  }

  componentDidMount() {
    // Randomise tiles on mount
    const sequence = this._shuffle(this.state.sequence)
    this.setState({ sequence })
  }

  handleClick(e) {
    let clickedNumber = parseInt(e.target.getAttribute("data-position"), 10)
    let clickedIndex = this.state.sequence.indexOf(clickedNumber)
    let emptySpace = this.state.sequence[8]

    if (this._allowedNumbers(emptySpace).indexOf(clickedNumber) !== -1) {
      // Swap postion in array
      let sequence = this.state.sequence
      sequence[clickedIndex] = this.state.sequence[8]
      sequence[8] = clickedNumber

      this.setState({ sequence })

      // Check if game is complete
      if (sequence.toString() === [1, 2, 3, 4, 5, 6, 7, 8, 9].toString()) {
        alert("Well done!")
      }
    }
  }

  render() {
    // Render all title minus one for the empty space
    let tiles = this.state.sequence.slice(0, 8)
    return (
      <div className="game">
        <div className="game--container">
          {tiles.map((num, index) => (
            <Tile key={index} num={num} onClick={this.handleClick.bind(this)} />
          ))}
        </div>
      </div>
    )
  }
}

export default Game
