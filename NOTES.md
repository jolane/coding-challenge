# Instructions

Click/touch the square/tile that you would like to move to the empty space.

# Future improvements

* Add drag and drop library to improve user experience.
* Add option for different size games e.g. 4x3, 4x4
* Add keyboard support
